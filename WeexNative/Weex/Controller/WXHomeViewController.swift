//
//  WXHomeViewController.swift
//  Mall
//
//  Created by Abhishek Shukla on 05/12/17.
//  Copyright © 2017 One97. All rights reserved.
//

import Foundation
import WeexSDK

class WXHomeViewController: UIViewController {
    var instance: WXSDKInstance?
    var weexView = UIView()
    var weexHeight:CGFloat?
    var sourceString: String?
    
    var options:[String : Any]?
    private var lastReloadedDate = Date()
    typealias returnTypeBlock = (_ created: Bool) -> Void
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerWeexEnvironment(url: WXNativeConstants.wxCommonUrl!) { (isRegistered) in
                if (isRegistered) {
                    self.createGoldViewController(WXNativeConstants.wxGoldUrl!, optionalUrl: "") { (isDone) in
                        if (isDone) {
                            self.render()
                        }
                    }
            }
        }
    }
    
    deinit {
        instance?.destroy()
    }
    
    private func render() {
        instance?.destroy()
        weak var weakSelf = self
        instance = WXSDKInstance()
        instance?.viewController = weakSelf

        instance?.onFailed = { (error:Error?)-> Void in
            print("WEEX HOME: - ON FAILED: \(error.debugDescription)")
        }

        instance?.renderFinish = { (view:UIView?)-> Void in
            print("Render Finished")
        }
        instance?.updateFinish = { (view:UIView?)-> Void in
            print("Update Finished")
        }
        
        instance?.onCreate = { (customView:UIView?)-> Void in
            if let view = customView, var weexView = weakSelf?.weexView {
                weexView.removeFromSuperview()
                weexView = view
                weakSelf?.view.addSubview(weexView)
                UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, weakSelf?.weexView)
            }
        }
        
//        let width = view.frame.size.width
//        var height = view.frame.size.height
//        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
//        instance?.frame = CGRect(x: 0, y: 0, width: width, height: height)
        
        if let sourceString = sourceString {
            instance?.renderView(sourceString, options: options, data: nil)
        }
    }
}

// MARK:- WEEX Pages - CLP, Grid, Flyout, Profile, PDP
extension WXHomeViewController {
    
    enum Componets: String {
        case progressBar = "progressBar"
        case richText = "richText"
        case webView = "webView"
        case youtube = "youtube"
        case editText = "editText"
    }
    
    var componetsToRegister: [Componets] {
        get {
            return [.progressBar, .richText, .webView, .youtube, .editText]
        }
    }
    
    typealias response = (_ isRegistered: Bool) -> Void
    
    func registerWeexEnvironment(url: String, completionBlock: @escaping response) {
        WXNativeNetworking().weexCacheJS(WXNativeConstants.wxCacheCommonJS, url: url) { [weak self] (jsCode) in
            if let jsCode = jsCode {
                let appVersion = ""
//                WXAppConfiguration.setAppGroup("One97")
//                WXAppConfiguration.setAppName("Mall")
//                WXAppConfiguration.setAppVersion(appVersion)
                WXSDKEngine.registerModule("oauth", with: NSClassFromString("WXEventModule"))
                WXSDKEngine.registerModule("nativeStream", with: NSClassFromString("WXNetworkRequest"))
//                WXSDKEngine.registerService("COMMON_SERVICE", withScript: jsCode, withOptions: nil)
                WXSDKEngine.registerService("COMMON_SERVICE", withScript: jsCode, withOptions: ["serviceName":"COMMON_SERVICE", "components": self?.getStringifyComponents() ?? "[]"])
//                                self?.registerComponents()
                WXSDKEngine.registerHandler(WXImageLoaderDefaultImplement(), with:NSProtocolFromString("WXImgLoaderProtocol"))
                WXSDKEngine.initSDKEnvironment()
                completionBlock(true)
            }
            else {
                completionBlock(false)
            }
        }
    }
    
    private func getStringifyComponents() -> String? {
        var componetsStringArray = [String]()
        for componet in componetsToRegister {
            componetsStringArray.append(componet.rawValue)
        }
        
        guard let data = try? JSONSerialization.data(withJSONObject: componetsStringArray, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    //    private func registerComponents() {
    //        for component in componetsToRegister {
    //            switch component {
    ////            case .progressBar:
    ////                WXSDKEngine.registerComponent(component.rawValue, with: WXProgress.self)
    ////            case .richText:
    ////                WXSDKEngine.registerComponent(component.rawValue, with: WXRichTextComponent.self)
    //            case .webView:
    ////                WXSDKEngine.registerComponent(component.rawValue, with: WXWebViewComponent.self)
    //            case .youtube:
    ////                WXSDKEngine.registerComponent(component.rawValue, with: WXYouTubeComponent.self)
    //            case .editText:
    ////                WXSDKEngine.registerComponent(component.rawValue, with: WXEditText.self)
    //            }
    //        }
    //    }
    public func createGoldViewController(_ goldUrl: String, optionalUrl: String, completion: @escaping returnTypeBlock) {
        WXNativeNetworking().weexCacheJS(WXNativeConstants.wxCacheGold, url: goldUrl) { (jsCode) in
            if let jsCode = jsCode {
                let wxEventModel = WXEventModel(goldUrl, isCachingRequired: false, isScrollRequired: false, isTitleRequired: false, optionalUrl: optionalUrl, route: "", isBarCodeEnabled: false, title: "")
//                let optionsDic = WXEventModel.prepareOptionDic(wxEventModel)
                let optionsDic = [String: String]()
                self.sourceString = jsCode
                self.options = optionsDic
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}



