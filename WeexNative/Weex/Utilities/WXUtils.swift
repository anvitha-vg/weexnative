//
//  WXUtils.swift
//  Jarvis
//
//  Created by Brammanand Soni on 18/05/18.
//  Copyright © 2018 One97. All rights reserved.
//

import UIKit

class WXUtils: NSObject {

    static func getModifiedUrlForDebugSettings(_ url: String) -> String {
        var modified = url
        #if DEBUG
        NSLog("\n========================================================")
        #if PAYTM_MALL && !APPSTORE_BUILD
        let debugObj = JRDebugSettingsViewModel()
        debugObj.getDebugSettingsArray()
        for obj in debugObj.debugSettingsArray {
            if !obj.isRuleMuted {
                if url.contains(obj.replaceURL) == false {
                    if url.contains(obj.findURL) == true {
                        if obj.isReplaceFullURL {
                            modified = obj.replaceURL
                        }
                        else {
                            modified = url.replacingOccurrences(of: obj.findURL, with: obj.replaceURL)
                        }
                    }
                }
                NSLog("\nRequesting with url: %@", modified)
            }
        }
        if debugObj.debugSettingsArray.count == 0 {
            NSLog("\nRequesting with url: %@", modified)
        }
        #else
        NSLog("\nRequesting with url: %@", modified)
        #endif
        #endif
        
        return modified
    }
    
}

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func isNumeric() -> Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    func isEqualToString(find: String) -> Bool {
        return String(format: self) == find
    }
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
}
