//
//  WXConstants.swift
//  Mall
//
//  Created by Abhishek Shukla on 15/01/18.
//  Copyright © 2018 One97. All rights reserved.
//

import Foundation

public struct WXNativeConstants {
    
    static let wxGetGTMValue = "GET_GTM_VALUE"
    static let wxGridGetPinCode = "GRID_GET_PINCODE"
    static let wxGridSetPinCode = "GRID_SET_PINCODE"
    static let wxGAEvent = "GA_EVENT"
    static let wxGridSetCartId = "GRID_SET_CART_ID"
    static let wxGridOpenMoreOffers = "GRID_OPEN_MORE_OFFERS"
    static let wxGridBarCodeClicked = "GRID_BARCODE_CLICKED"
    static let wxGridSearchClick = "GRID_SEARCH_CLICK"
    static let wxGridCartClick = "GRID_CART_CLICK"
    static let wxGridItemClick = "GRID_ITEM_CLICK"
    static let wxGridBackPressed = "GRID_BACK_PRESSED"
    static let wxNativeBackPressed="NATIVE_BACK_PRESSED"
    static let wxGridSetCartCount = "GRID_SET_CART_COUNT"
    static let wxGridUpdateCartCount = "GRID_UPDATE_CART_COUNT"
    static let wxOpenFullFlyout = "OPEN_FULL_PAGE_FLYOUT"
    static let wxGridUpdateCartProductCount = "GRID_UPDATE_CART_PRODUCT_COUNT"
    static let wxGridToastOfCartUpadate = "GRID_TOAST_OF_CART_UPDATE"
    static let wxGridUpdateShoppingCart = "UPDATE_SHOPPING_CART"
    static let wxGridSearchData = "GET_SEARCH_DATA"
    static let wxMyOrdersClicked = "MYORDERS_CLICKED"
    static let wxContactsUsClicked = "CONTACTUS_CLICKED"
    static let wxSavedItemsClicked = "SAVED_ITEMS_CLICKED"
    static let wxMenuCreateAccount = "MENU_CREATE_ACCOUNT"
    static let wxPageRefresh = "PAGE_REFRESH"
    static let wxGridOpenLogin = "GRID_OPEN_LOGIN"
    static let wxEventReloadFlyout = "RELOAD_FLYOUT"
    static let wxReloadProfile = "RELOAD_PROFILE"
    static let wxActionUpdateProfile = "ACTION_UPDATE_PROFILE"
    /*
     - Profile VC Events
     */
    static let wxNotificationsClicked = "NOTIFICATIONS_CLICKED"
    static let wxHelpCenterClicked = "HELP_CENTER_CLICKED"
    static let wxSignOutClicked = "SIGNOUT_CLICKED"
    static let wxSettingClicked = "SETTINGS_CLICKED"
    static let wxGetProfileData = "GET_PROFILE_DATA"
    static let wxMenuProfileClicked = "MENU_PROFILE_CLICK"
    static let wxProfileBackPressed = "PROFILE_BACK_PRESSED"
    static let wxViewPassBookClicked = "VIEW_PASSBOOK_CLICKED"
    static let wxUserNameEditClicked = "USERNAME_EDIT_CLICKED"
    static let wxLuckyLifafaClicked = "LUCKYLIFAFA_CLICKED"
    static let wxInviteClicked = "INVITE_CLICKED"
    static let wxManageAppLockClicked = "MANAGE_APP_LOCK_CLICKED"
    static let wxChangePasswordClicked = "CHANGE_PASSWORD_CLICKED"
    static let wxPaytmAssistClicked = "PAYTM_ASSIST_CLICKED"
    static let wxChooseLanguageClicked = "CHOOSE_LANGUAGE_CLICKED"
    static let wxNotificationSettingsClicked = "NOTIFICATION_SETTINGS_CLICKED"
    static let wxDeliveryAddressClicked = "DELIVERY_ADDRESS_CLICKED"
    static let wxSettingsBackIconClicked = "SETTINGS_BACK_ICON_CLICKED"
    static let wxTrackOrderClicked = "TRACK_ORDER_CLICKED"
    
    /*
     - JS PDP Events
    */
    static let wxShareProdutClicked = "SHARE_PRODUCT_CLICKED"
    static let wxOpenWishList = "OPEN_WISHLIST"
    static let wxImeiScanClicked = "IMEI_SCAN_CLICKED"
    static let wxOpenMoreSeller = "OPEN_MORE_SELLER"
    static let wxNOEMIData = "NO_EMI_DATA"
    static let wxZoomImageClicked = "ZOOM_IMAGE_CLICKED"
    static let wxGetTrackingInfo = "GET_TRACKING_INFO"
    static let wxGetSelectedAddress = "GET_SELECTED_ADDRESS"
    static let wxSetSelectedAddress = "SET_SELECTED_ADDRESS"
    static let wxNumberClicked = "NUMBER_CLICKED"
    static let wxExpressCheckout = "EXPRESS_CHECKOUT"
    static let wxDebugModeClicked = "DEBUG_MODE_CLICKED"
    static let wxVideoOutOfFocus = "VIDEO_OUT_OF_FOCUS"
    
    /*
     - JS Events
     */
    static let wxCartVisiableEvent = "CART_VISIBLE_EVENT"
    
    static let wxActionUpdateWishList = "ACTION_UPDATE_WISHLIST"
    static let wxActionUpdateCart = "ACTION_UPDATE_CART"
    
    /*
     - JS Cache Keys
     */
    static let wxCacheCommonJS  = "common_js"
    static let wxCacheCLPJS     = "clp_js"
    static let wxCacheGRIDJS    = "grid_js"
    static let wxCacheMenuJS    = "menu_js"
    static let wxProfileJS      = "profile_js"
    static let wxPDPJS          = "pdp_js"
    static let wxCacheGold           = "gold_js"
    /*
     - Notification
     */
    static let pauseYoutubeVideoNotification  = "pause_youtube_video_notification"
    
    // Remove Serviceability
    static let wxGridGetUserName = "GRID_GET_USERNAME"
    static let wxGridSetUserName = "GRID_SET_USERNAME"
    static let wxGridGetCityName = "GRID_GET_CITY_NAME"
    static let wxGridSetCityName = "GRID_SET_CITY_NAME"
    // Remove Serviceability
}

extension WXNativeConstants {
//    static let folder = "release-weex-1.6.0"
//    static let urlPrefix = "http://assets.paytm.com/dexter/weex/"
//    static let goldUrl = "https://dg-static1.paytm.com/weex/gold.js"
//    static let goldUrl = "http://10.11.33.1/gold.js"
    static var wxCommonUrl: String? {
//        return "\(urlPrefix)\(folder)/common-service.js"
//        return "http://dg-static.paytm.com/weex/common-service.js"
        return "http://10.11.33.1/common-service.js"
    }
//    static var wxCLPUrl: String? {
//        return "\(urlPrefix)\(folder)/home.js"
//    }
//    static var wxGridUrl: String? {
//        return "\(urlPrefix)\(folder)/grid.js"
//    }
//    static var wxProfileUrl: String? {
//        return "\(urlPrefix)\(folder)/profile.js"
//    }
//    static var wxMenuUrl: String? {
//        return "\(urlPrefix)\(folder)/menu.js"
//    }
//    static var wxProductUrl: String? {
//        return "\(urlPrefix)\(folder)/product.js"
//    }
    static var wxGoldUrl: String? {
        return "http://10.11.33.1/gold.js"
    }
}

