//
//  WXNativeNetworking.swift
//  Mall
//
//  Created by Abhishek Shukla on 31/01/18.
//  Copyright © 2018 One97. All rights reserved.
//

import Foundation
import JavaScriptCore

class WXNativeNetworking {
    
    typealias jsScript = (_ value: String?) -> Void
    
    typealias status = (_ value: Bool) -> Void

    func weexCacheJS(_ cacheName: String, url: String, completionHandler: @escaping jsScript) {
        let modified = WXUtils.getModifiedUrlForDebugSettings(url)
        
        let cache = Cache<String>(name: cacheName)
        if let urlString = URL(string: modified){
            cache.fetch(URL: urlString).onSuccess { jsCode in
                completionHandler(jsCode)
            }
            cache.fetch(URL: urlString).onFailure{_ in
                completionHandler(nil)
            }
        } else {
            completionHandler(nil)
        }
        
        needTOExpireCache(cacheName, url: modified)
    }
    
    func needTOExpireCache(_ cacheName: String, url: String) {
    
        //ETAG
        guard let request = httpRequestURL(urlString: url) else {
            return
        }
        
        URLSession.shared.dataTask(with: request) { (jsonData, response, error) in
            if error ==  nil {
                if let httpResponse = response as? HTTPURLResponse {
                  if httpResponse.statusCode != 304, let eTag = httpResponse.allHeaderFields["Etag"] as? String {
                        UserDefaults.standard.setValue(eTag, forKey: url)
                        UserDefaults.standard.synchronize()
                        let cache = Cache<String>(name: cacheName)
                        cache.remove(key: url)
                    }
                }
            }
        }
    }
    
    func httpRequestURL(urlString: String?) -> URLRequest? {
        
        guard let unwrappedUrl = urlString else {
            return nil
        }
        guard let stringUrl = URL(string: unwrappedUrl) else{
            return nil
        }
        
        var request = URLRequest.init(url: stringUrl)
        request.httpMethod = "GET"
        if let savedETAG = UserDefaults.standard.value(forKey: unwrappedUrl) as? String {
            request.addValue(savedETAG, forHTTPHeaderField: "If-None-Match")
        }
        return request
    }
}
