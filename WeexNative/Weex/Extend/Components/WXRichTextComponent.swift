//
//  WXRichTextComponent.swift
//  Jarvis
//
//  Created by Brammanand Soni on 05/06/18.
//  Copyright © 2018 One97. All rights reserved.
//

import UIKit

//class WXRichTextComponent: WXComponent {

//    var attributedText: NSAttributedString?
//
//    override public func loadView() -> UIView {
//        let label = UILabel()
//        label.numberOfLines = 0
//        label.attributedText = attributedText
//        label.sizeToFit()
//        label.adjustsFontSizeToFitWidth = true
//        return label
//    }
//
//    override init(ref: String, type: String, styles: [AnyHashable : Any]?, attributes: [AnyHashable : Any]? = nil, events: [Any]?, weexInstance: WXSDKInstance) {
//        super.init(ref: ref, type: type, styles: styles, attributes: attributes, events: events, weexInstance: weexInstance)
//        var styleStr: String = ""
//        if let fontFamily = attributes?["fontFamily"] as? String {
//            styleStr = "font-family:'\(fontFamily)';"
//        }
//        if let styleDict = styles, styleDict.keys.count > 0 {
//            if let fontSize = styleDict["fontSize"] as? Float {
//                styleStr = styleStr + "font-size:\(fontSize * Float(weexInstance.pixelScaleFactor));"
//            }
//            if let color = styleDict["color"] as? String {
//                styleStr = styleStr + "color:\(color);"
//            }
//            if let fontWeight = styleDict["fontWeight"] as? Int {
//                styleStr = styleStr + "font-weight:\(fontWeight);"
//            }
//        }
//        if let richText = attributes?["value"] as? String {
//            var htmlString = richText
//            if !richText.contain(subStr: "<html>") {
//                htmlString = "<html><body style=\"\(styleStr)\">\(richText)<body></html>"
//            }
//            attributedText = htmlString.htmlToAttributedString
//        }
//    }
//
//    override public func measureBlock() -> ((CGSize) -> CGSize)? {
//        super.measureBlock()
//
//        func layout(_ size: CGSize) -> CGSize {
//            let height = attributedText?.height(withConstrainedWidth: size.width) ?? 0
//            return  CGSize(width: size.width, height: height)
//        }
//        return layout
//    }
//}
//
//extension String {
//    var htmlToAttributedString: NSAttributedString? {
//        guard let data = data(using: .utf8) else { return NSAttributedString() }
//        do {
//            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
//        } catch {
//            return NSAttributedString()
//        }
//    }
//
//    var htmlToString: String {
//        return htmlToAttributedString?.string ?? ""
//    }
//}
//
//extension NSAttributedString {
//    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
//        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
//
//        return ceil(boundingBox.height)
//    }
//
//    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
//        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
//        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
//
//        return ceil(boundingBox.width)
//    }
//}
