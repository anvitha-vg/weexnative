//
//  JRYoutubePlayerView.swift
//  Jarvis
//
//  Created by Brammanand Soni on 06/06/18.
//  Copyright © 2018 One97. All rights reserved.
//

import UIKit

//class JRYoutubePlayerView: UIView {
//    @IBOutlet weak var thumbnailImageView: UIImageView!
//
//    let youtubePlayerViewTag = 9988
//
//    private var mediaItem: JRProductMedia!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        NotificationCenter.default.addObserver(self, selector: #selector(pauseVideo), name: NSNotification.Name(rawValue: WXNativeConstants.pauseYoutubeVideoNotification), object: nil)
//    }
//
//    deinit {
//        NotificationCenter.default.removeObserver(self)
//    }
//
//    static func loadYoutubePlayerView() -> JRYoutubePlayerView {
//        return Bundle.main.loadNibNamed("JRYoutubePlayerView", owner: self, options: nil)?.last as! JRYoutubePlayerView
//    }
//
//    func setupMediaItem(_ mediaItem: JRProductMedia?) {
//        if let mediaObj = mediaItem {
//            self.mediaItem = mediaObj
//            let imageurl = JRImageUtilities.urlForImage(mediaObj.imageURL, size: CGSize(width: JRSwiftConstants.windowWidth, height: 0.0))
//            thumbnailImageView.sd_setImage(with: URL(string: imageurl))
//            addPlayButtonView(view: thumbnailImageView)
//        }
//    }
//
//    private func addPlayButtonView(view: UIView) {
//        let blackView = UIView(frame: CGRect(x: 0, y: 0, width: 130, height: 42))
//        blackView.backgroundColor =  UIColor.colorRGB(34, g: 34, b: 34, a: 0.9)
//        blackView.isUserInteractionEnabled = true
//        blackView.layer.cornerRadius = 21
//
//        let arrowImage = UIImageView(frame: CGRect(x: 14, y: 12.5, width: 11, height: 17))
//        arrowImage.image = UIImage(named: "pdpvideoplay")
//        blackView.addSubview(arrowImage)
//
//
//        let titleLabel = UILabel(frame: CGRect(x: 33, y: 9.5, width: 82, height: 23))
//        titleLabel.textColor = UIColor.white
//        titleLabel.text = "Play Video"
//        titleLabel.font = UIFont.fontBoldOf(size: 15)
//        blackView.addSubview(titleLabel)
//
//        blackView.center = view.center
//        view.addSubview(blackView)
//        addTapGesture(view)
//    }
//
//    private func addTapGesture(_ view: UIView) {
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(playVideo))
//        view.isUserInteractionEnabled = true
//        view.addGestureRecognizer(tapGesture)
//    }
//
//    @objc func playVideo() {
//        JRPDPVideoPlayer.setupYouTubeVideoPlayer(owner:self, superView: thumbnailImageView, videoURL: mediaItem.VideoURL, tag: youtubePlayerViewTag, offset:0, frame:thumbnailImageView.frame)
//    }
//
//    @objc private func pauseVideo() {
//        let payerView = thumbnailImageView.viewWithTag(youtubePlayerViewTag) as? YTPlayerView
//        payerView?.pauseVideo()
//    }
//
//    private func removeYoutubePlayerView() {
//        let payerView = thumbnailImageView.viewWithTag(youtubePlayerViewTag)
//        payerView?.removeFromSuperview()
//    }
//}
//
//extension JRYoutubePlayerView: YTPlayerViewDelegate {
//    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
//        playerView.playVideo()
//    }
//
//    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
//        if state == .ended {
//            removeYoutubePlayerView()
//        }
//    }
//
//    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
//        removeYoutubePlayerView()
//    }
//}
