//
//  WXNetworkErrorModel.swift
//  Jarvis
//
//  Created by Abhishek on 27/05/18.
//  Copyright © 2018 One97. All rights reserved.
//

import Foundation

public struct WXNetworkErrorModel {

    let error: String?
    let error_title: String?
    let errorStatus: WXErrorStatus
    let code: Int
    let errorCode: String?
}

extension WXNetworkErrorModel: Decodable {
    
    private enum WXNetworkErrorResponseCodingKeys: String, CodingKey {
        case error_title
        case errorStatus = "status"
        case errorCode
        case code
        case error
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: WXNetworkErrorResponseCodingKeys.self)
        error_title = try container.decode(String.self, forKey: .error_title)
        errorStatus = try container.decode(WXErrorStatus.self, forKey: .errorStatus)
        errorCode = try container.decode(String.self, forKey: .errorCode)
        code = try container.decode(Int.self, forKey: .code)
        error = try container.decode(String.self, forKey: .error)
    }
}

struct WXErrorStatus: Decodable {
    
    let result: String?
    let message: WXErrorMessage
    
    private enum WXErrorStatusResponseKey: String, CodingKey {
        case result
        case message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: WXErrorStatusResponseKey.self)
        
        result = try container.decode(String.self, forKey: .result)
        message = try container.decode(WXErrorMessage.self, forKey: .message)
    }
}

struct WXErrorMessage: Decodable {
    
    let title: String?
    let message: String?
    
    private enum WXErrorMessageResponseKeys: String, CodingKey {
        case title
        case message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: WXErrorMessageResponseKeys.self)

        title = try container.decode(String.self, forKey: .title)
        message = try container.decode(String.self, forKey: .message)
    }
}


