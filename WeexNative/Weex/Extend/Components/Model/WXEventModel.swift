//
//  WXEventModel.swift
//  Mall
//
//  Created by Abhishek Shukla on 06/12/17.
//  Copyright © 2017 One97. All rights reserved.
//

import Foundation

public struct WXEventModel {
    var bundleUrl: String?
    var isCachingRequired: Bool?
    var isScrollRequired: Bool?
    var isTitleRequired: Bool?
    var optionalUrl: String?
    var route: String?
    var isBarCodeEnabled: Bool?
    var title: String?
    
    init(_ bundleUrl: String, isCachingRequired: Bool, isScrollRequired: Bool, isTitleRequired: Bool, optionalUrl: String, route: String, isBarCodeEnabled: Bool, title: String) {
        self.bundleUrl = bundleUrl
        self.isCachingRequired = isCachingRequired
        self.isScrollRequired = isScrollRequired
        self.isTitleRequired = isTitleRequired
        self.optionalUrl = optionalUrl
        self.route = route
        self.isBarCodeEnabled = isBarCodeEnabled
        self.title = title
    }
    
    static func prepareOptionDic(_ wxModel: WXEventModel) -> [String: Any] {
        var optionDic = [String: Any]()
        
        if let route = wxModel.route {
            optionDic["route"] = route
        }
        if let bundleUrl = wxModel.bundleUrl {
            optionDic["bundle_url"] = bundleUrl
        }
        if let isCachingRequried = wxModel.isCachingRequired {
            optionDic["is_caching_required"] = isCachingRequried
        }
        if let isScrollEventRequried = wxModel.isScrollRequired {
            optionDic["scroll_event_required"] = isScrollEventRequried
        }
        if let isTitleRequried = wxModel.isTitleRequired {
            optionDic["is_title_required"] = isTitleRequried
        }
        if let optionalURL = wxModel.optionalUrl?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
            let modifiedOptionalUrl = WXUtils.getModifiedUrlForDebugSettings(optionalURL)
            let decodedUrl = modifiedOptionalUrl.removingPercentEncoding
            if modifiedOptionalUrl == decodedUrl {
                optionDic["url"] = modifiedOptionalUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            } else {
                optionDic["url"] = modifiedOptionalUrl
            }
        }
        if let isBarCodeEnabled = wxModel.isBarCodeEnabled, isBarCodeEnabled == true {
            optionDic["enable_barcode"] = isBarCodeEnabled
        }
        if let title = wxModel.title {
            optionDic["title"] = title
        }
        
        optionDic["is_native_stream_available"] = true
        return optionDic
    }
}

