//
//  WXWebViewComponent.swift
//  Jarvis
//
//  Created by Brammanand Soni on 06/06/18.
//  Copyright © 2018 One97. All rights reserved.
//

import UIKit

//class WXWebViewComponent: WXComponent, UIWebViewDelegate {
//    
//    private var htmlString: String?
//
//    override func loadView() -> UIView {
//        let webView = UIWebView()
//        webView.delegate = self
//        webView.loadHTMLString(htmlString ?? "", baseURL: nil)
//        return webView
//    }
//    
//    override init(ref: String, type: String, styles: [AnyHashable : Any]?, attributes: [AnyHashable : Any]? = nil, events: [Any]?, weexInstance: WXSDKInstance) {
//        super.init(ref: ref, type: type, styles: styles, attributes: attributes, events: events, weexInstance: weexInstance)
//        if let text = attributes?["value"] as? String {
//            htmlString = text
//        }
//    }
//    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        webView.stringByEvaluatingJavaScript(from: "document.body.style.fontFamily = 'HelveticaNeue'")
//    }
//}
