//
//  WXEventModule.swift
//  Mall
//
//  Created by Abhishek Shukla on 16/11/17.
//  Copyright © 2017 One97. All rights reserved.
//

import Foundation
import WeexSDK

//MARK:- Exposed Methods
public extension WXEventModule {
    
    @objc public func openUrl(_ url:String, urlType: String) {
//        var urlType = urlType
//        if urlType.isEmpty {
//            urlType = JRDeeplinkHandler.synonymFor(urlType: url)
//        }
//
//        if urlType == "product" {
//            if let baseViewController = UIViewController.baseControllerForTab(UIViewController.selectedTab()) ,  let mallVC = baseViewController.topViewController as? JRBaseViewController, let deepLinkInfo = mallVC.deepLinkInfo {
//                if let scan_id = deepLinkInfo["order_id"] as? String {
//                    let modifiedUrl = url + "&scan_id=\(scan_id)"
//                    handleOpenUrl(modifiedUrl, urlType: urlType)
//                    return
//                }
//            }
//        }
//         handleOpenUrl(url, urlType: urlType)
    }
    
    @objc public func weexPageScrollEvent(_ pageTag: String, x: Int, y: Int) {
        #if PAYTM_MALL
            if let landingPageVC = self.weexInstance.viewController as? JRRMallLandingPageViewcontroller {
                landingPageVC.setNavigationTitleViewColor(offset: CGFloat(y.magnitude))
                landingPageVC.setTopBarBackgroundColor(offset: CGFloat(y.magnitude))
            }
        #endif
    }
    
    @objc public func sendGAEvent(_ event: String, dict: [String: AnyObject]) {
        
    }
    
    @objc public func sendGAEventWithMapOnly(_ dict: [String : AnyObject]) {
        
    }
    
    @objc public func log(_ tag: String, messages: [AnyObject]) {
        #if !APPSTORE_BUILD
        print("Tag: \(tag) and messages: \(messages)")
        #endif
    }
    
    @objc public func getAppContextDetails(_ callback: WXModuleCallback) {
        callback(getContextDetail())
    }
    
    @objc public func weexPageTitleChangeEvent(_ key: String, title: String) {
        if let weexCLP = self.weexInstance.viewController as? WXHomeViewController {
           
        }
    }
    
    @objc public func getResponseData(_ url: String, jsonData:String) {
//        if let weexVC = self.weexInstance.viewController as? WXHomeViewController {
//            if let dict = convertToDictionary(text: jsonData) {
//                let urlType = JRDeeplinkHandler.synonymFor(urlType: url)
//                if urlType == JRDeeplinkHandler.urlTypeProduct { // PDP response
//                    if let pdpResponse = dict as? [String : Any] {
//                        let product = JRProduct(dictionary: pdpResponse, isForPDP: true)
//                        weexVC.productDetails = product
//                    }
//                }
//                    // TODO: Check here url type and perform action.
//                else if let contextParamDict = dict["contextParams"] as? [AnyHashable: Any] {
//                    weexVC.clpContext = contextParamDict
//                    weexVC.placeholder = ""
//                    if let brandName = contextParamDict["store_name"] as? String {
//                        weexVC.placeholder = brandName
//                    }
//                }
//            }
//        }
    }
    
    @objc public func showProgressBar(_ key: String, gravity :Int, showProgress: Bool, delayMillis: Int) {
//        if let weexVC = self.weexInstance.viewController as? WXHomeViewController {
//            if showProgress {
//                weexVC.view.bringSubview(toFront: weexVC.activityIndicatorView)
//                weexVC.activityIndicatorView.startAnimating()
//            } else {
//                weexVC.activityIndicatorView.stopAnimating()
//            }
//        }
    }
    
    @objc public func updateGAKey(_ pageUrl: String, gaKey: String) {
    }
    
    @objc public func openUrlWithMapParams(_ param: [String: String]) {
    }
    
    @objc public func genericNativeCall(_ event: String, dict: [String: AnyObject], callback: @escaping WXModuleCallback) {
//        switch (event) {
//
//        //MARK:- Flyout Listeners
//        case WXNativeConstants.wxMenuProfileClicked:
//            dismissFlyout()
//            navigateToProfileVC(0)
//            break
//        case WXNativeConstants.wxMenuCreateAccount:
//            dismissFlyout()
//            if let baseViewController = UIViewController.baseControllerForTab(UIViewController.selectedTab()) ,  let mallVC = baseViewController.topViewController as? JRBaseViewController {
//                mallVC.launchLoginViewWithSignUpScreenAtFrontDue(toAuthenticationError: false, withError: nil, withTitle: "Sign Up", bySkippingStep2: false)
//            }
//            break
//        case WXNativeConstants.wxSignOutClicked:
//            JRAccount.shared().signOut()
//            if let weexVC = self.weexInstance.viewController as? WXHomeViewController {
//                weexVC.navigationController?.popToRootViewController(animated: true)
//            }
//            break
//        case WXNativeConstants.wxOpenFullFlyout:
//            showFlyout(dict: dict)
//            break
//
//        //MARK:- Profile Listeners
//        case WXNativeConstants.wxProfileBackPressed:
//            popWeexVC()
//            break
//        case WXNativeConstants.wxUserNameEditClicked:
//            if let weexVC = self.weexInstance.viewController as? WXHomeViewController {
//                let settingsVC = JREditProfileViewController.controller()
//                weexVC.navigationController?.navigationBar.tintColor = .black
//                weexVC.navigationController?.pushViewController(settingsVC, animated: true)
//            }
//            break
//
//        case WXNativeConstants.wxViewPassBookClicked:
//            // FIXME:- Pass Book
//            if let weexVC = self.weexInstance.viewController as? WXHomeViewController {
//                let passBookVC = JRPassbookUtils.passbookLanding
//                weexVC.navigationController?.pushViewController(passBookVC, animated: true)
//            }
//            break
//        case WXNativeConstants.wxMyOrdersClicked:
//            #if PAYTM_MALL
//              navigateToMyOrders()
//            #endif
//            break
//        case WXNativeConstants.wxHelpCenterClicked:
//            moveTo(urlString: "paytmmall://contactus")
//            break
//        case WXNativeConstants.wxNotificationsClicked:
//            navigateToUpdates()
//            break
//        case WXNativeConstants.wxSettingClicked:
//            navigateToProfileVC(1)
//            break
//        case WXNativeConstants.wxTrackOrderClicked:
//            loadOrderSummary(dict)
//            break
//
//        // MARK:- Settings Listeners
//        case WXNativeConstants.wxSettingsBackIconClicked:
//            popWeexVC()
//            break
//        case WXNativeConstants.wxLuckyLifafaClicked:
//            #if PAYTM_MALL
//                navigateToLuckyLifafa()
//            #endif
//            break
//        case WXNativeConstants.wxDeliveryAddressClicked:
//            navigateToDeliveryAddress()
//            break
//        case WXNativeConstants.wxInviteClicked:
//            loadInviteAction()
//            break
//        case WXNativeConstants.wxNotificationSettingsClicked:
//            navigateToSettingsNotifications()
//            break
//        case WXNativeConstants.wxChooseLanguageClicked:
//            break
//        case WXNativeConstants.wxPaytmAssistClicked:
//            break
//        case WXNativeConstants.wxChangePasswordClicked:
//            navigateToChangePassword()
//            break
//        case WXNativeConstants.wxManageAppLockClicked:
//            navigateToChangeAppLock()
//            break
//
//
//
//        //MARK:- JS CallBack
//        case WXNativeConstants.wxGetProfileData:
//            if let profileData = getProfileData() {
//                callback(profileData)
//                break
//            }
//
//
//        //MARK:- GA Listeners
//        case WXNativeConstants.wxGAEvent:
//
//            guard let eventName = dict["event_name"] as? String else {
//                return
//            }
//            guard let gaDict = convertToDictionary(text: dict["ga_data"] as! String) as? [String: AnyObject] else {
//                return
//            }
//
//            if ("custom_event").isEqualToString(find: eventName) {
//                sendGAEventWithMapOnly(gaDict)
//            } else {
//                sendGAEvent(eventName, dict: gaDict)
//            }
//            break
//
//        case WXNativeConstants.wxGetTrackingInfo:
//            // TODO:- Get Tracking Info
//            hanldeExpressCheckoutTrackingInfo(dict: dict, callback: callback)
//            break
//
//        //MARK:- Seach Listeners
//        case WXNativeConstants.wxGridSearchClick:
//            if let weexCLP = self.weexInstance.viewController as? WXHomeViewController {
//                if let contextParamDict = dict["contextparams"] as? String {
//                    if let context = convertToDictionary(text: contextParamDict) {
//                        weexCLP.clpContext = context
//                        weexCLP.placeholder = ""
//                    }
//                }
//                UIViewController.presentSearchViewController(weexCLP, withQueryString: "")
//            }
//            break
//
//
//        //MARK:- CART Listeners
//        case WXNativeConstants.wxGridCartClick:
//            navigateToCartViewController()
//            break
//
//        //MARK:- Grid Listeners
//        case WXNativeConstants.wxGridBackPressed:
//            if let keyboardFlag = dict.getOptionalBoolKey("keyboardFlag"), keyboardFlag {
//                if let weexVC = self.weexInstance.viewController as? WXHomeViewController {
////                    weexVC.resignFirstResponder()
//                }
//            } else {
//                if let weexCLP = self.weexInstance.viewController as? WXHomeViewController {
//                    guard let navigation = weexCLP.navigationController as? JRNavigationController else {
//                        return
//                    }
//                    navigation.popViewController(animated: true)
//                }
//            }
//
//            break
//
//        case WXNativeConstants.wxGridItemClick:
//            guard let productObject = convertToDictionary(text: dict["product"] as! String) else {
//                return
//            }
//
//            guard let productUrl = productObject["url"] as? String else {
//                return
//            }
//
//            openUrl(productUrl, urlType: "")
//            break
//
//        case WXNativeConstants.wxGridSetCartCount:
//            updateCart(dict: dict)
//            break
//
//        case WXNativeConstants.wxGridUpdateShoppingCart:
//            updateCart(dict: dict)
//            break
//
//        case WXNativeConstants.wxGridToastOfCartUpadate:
//            break
//
//        case WXNativeConstants.wxGridOpenLogin:
//            dismissFlyout()
//            openLoginView()
//            break
//
//        case WXNativeConstants.wxGridSetCartId:
//            break
//
//        case WXNativeConstants.wxGridSearchData:
//            if let weexVC = self.weexInstance.viewController as? WXHomeViewController, let searchType = weexVC.searchType {
//                callback(["searchType": searchType]);
//            } else {
//                callback(["searchType": ""]);
//            }
//            break
//
//        case WXNativeConstants.wxGridGetPinCode:
//            let pinCode = PDPCheckDeliveryViewController.getPinCode()
//            callback(["pincode": pinCode ?? ""])
//            break
//
//        case WXNativeConstants.wxGridSetPinCode:
//            let pincode = dict["pincode"] as? String
//            if let code = pincode {
//                UserDefaults.standard.setValue(code, forKey: JRUserPreferancePinCode)
//                UserDefaults.standard.synchronize()
//                JRAddressManager.sharedInstance().resetAddressesForServiceabilityCheck()
//
//            }
//            break
//
//        // Remove Serviceability
//        case WXNativeConstants.wxGridGetUserName:
//            let userName = UserDefaults.standard.object(forKey: JRUserPreferanceUserName) as? String
//            callback(["username": userName ?? ""])
//            break
//
//        case WXNativeConstants.wxGridSetUserName:
//            let userName = dict["username"] as? String
//            if let userName = userName {
//                UserDefaults.standard.setValue(userName, forKey: JRUserPreferanceUserName)
//                UserDefaults.standard.synchronize()
//            }
//            break
//
//        case WXNativeConstants.wxGridGetCityName:
//            let cityName = UserDefaults.standard.object(forKey: JRUserPreferanceCity) as? String
//            callback(["cityName": cityName ?? ""])
//            break
//
//        case WXNativeConstants.wxGridSetCityName:
//            let cityName = dict["cityName"] as? String
//            if let cityName = cityName {
//                UserDefaults.standard.setValue(cityName, forKey: JRUserPreferanceCity)
//                UserDefaults.standard.synchronize()
//            }
//            break
//            // Remove Serviceability
//
//        case WXNativeConstants.wxGridOpenMoreOffers:
//            navigateToOfferController(dict, callback: callback)
//            break
//
//        case WXNativeConstants.wxGridBarCodeClicked:
//            guard let weexCLP = self.weexInstance.viewController as? WXHomeViewController else {
//                return
//            }
//            guard let navigation = weexCLP.navigationController as? JRNavigationController else {
//                return
//            }
//            navigation.handleBarCodeClickEvent()
//            break
//
//
//        //MARK: GTM Key Listeners
//        case WXNativeConstants.wxGetGTMValue:
//            if dict["gtmKey"] != nil && dict["valueType"] != nil && dict["container"] != nil
//            {
//                if let container = dict["container"] as? Int {
//                    switch (container) {
//                    case 1:
//                        if let valueType = dict.getOptionalStringForKey("valueType"), let gtmKey = dict.getOptionalStringForKey("gtmKey") {
//                            let value = getValueFromGTMContainer1(valueType: valueType.uppercased(), key: gtmKey)
//                            callback(["value": value ?? ""])
//                        } else {
//                            callback(["value": ""])
//                        }
//                        break
//                    case 2:
//                        if let valueType = dict.getOptionalStringForKey("valueType"), let gtmKey = dict.getOptionalStringForKey("gtmKey") {
//                            let value = getValueFromGTMContainer2(valueType: valueType.uppercased(), key: gtmKey)
//                            callback(["value": value ?? ""])
//                        } else {
//                            callback(["value": ""])
//                        }
//                        break
//                    default:
//                        callback(["value": ""])
//                        break
//                    }
//                }
//            }
//            break
//
//
//        //MARK:- Unknown Listeners
//        case WXNativeConstants.wxSavedItemsClicked:
//            moveTo(urlString: "paytmmall://wishlist")
//            break
//        case WXNativeConstants.wxContactsUsClicked:
//            moveTo(urlString: "paytmmall://contactus")
//            break
//        case WXNativeConstants.wxShareProdutClicked:
//            if let weexVC = self.weexInstance.viewController as? WXHomeViewController, let product = weexVC.productDetails {
//                let message = "Check out \(product.name ?? "") on Paytm mall"
//                JRSocialShareManager.sharedInstance.launchShareViewWithTypeWithImagePreference(.othersShareOption, message: message, contentUrl: product.productShareUrlKey, gaKey: weexVC.gaKey, emailSubject: product.name ?? "")
//            }
//            break
//        case WXNativeConstants.wxOpenWishList:
//            openWishlistViewController()
//            break
//        case WXNativeConstants.wxImeiScanClicked:
//            openImeiScanScreen { (scanCode) in
//                callback(["scanData": scanCode])
//            }
//            break
//        case WXNativeConstants.wxOpenMoreSeller:
//            openMoreSellerScreen(callback)
//            break
//
//        //MARK:- PDP Listeners
//        case WXNativeConstants.wxExpressCheckout:
//            // TODO:- Express Checkout
//            handleExpressCheckout(dict: dict, callback: callback)
//            break
//        case WXNativeConstants.wxGetSelectedAddress:
//            handleGetSelectedAddress(dict: dict, callback: callback)
//            break
//        case WXNativeConstants.wxSetSelectedAddress:
//            handleSetSelectedAddress(dict: dict, callback: callback)
//            break
//        case WXNativeConstants.wxNumberClicked:
//            handleNumberClicked(dict: dict)
//            break
//        case WXNativeConstants.wxNOEMIData:
//            handleNoEMIData(dict: dict, callback: callback)
//            break
//
//        case WXNativeConstants.wxZoomImageClicked:
//            handleProductZoom(dict: dict)
//            break
//
//        case WXNativeConstants.wxDebugModeClicked:
//            openDebugSettings()
//            break
//        case WXNativeConstants.wxVideoOutOfFocus:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: WXNativeConstants.pauseYoutubeVideoNotification), object: nil)
//            break
//
//        default:
//            break
//        }
    }
}

//MARK:- Helper Methods
fileprivate extension WXEventModule {
    fileprivate func handleOpenUrl(_ url: String, urlType: String) {
//        var newUrl:String = url
//        if url.hasPrefix("//") {
//            newUrl = String.init(format: "http://%@", url)
//        }
//        initiateUserInteractionTimer()
//        let modifiedURL = newUrl.lowercased()
//        if modifiedURL.range(of:"javascript://") != nil {
//            return //NO URL BANNER
//        }
//        else if let appDelegate = UIApplication.shared.delegate as? JRAppDelegate {
//            var deeplinkInfo: [AnyHashable: Any] = [AnyHashable: Any]()
//            if let info = JRDeeplinkHandler.deeplinkFor(string: newUrl) {
//                deeplinkInfo = info
//            }
//            deeplinkInfo[JRDeeplinkHandler.deeplinkKeyUrlType] = urlType
//            deeplinkInfo[JRDeeplinkHandler.deeplinkKeyUrl] = newUrl
//            appDelegate.setDeepLinkingInformation(deeplinkInfo)
//        }
    }
    
    fileprivate func initiateUserInteractionTimer() {
        self.weexInstance.viewController.view.isUserInteractionEnabled = false
        Timer.scheduledTimer(timeInterval: 1, target: self, selector:#selector(enableView), userInfo: nil, repeats: false)
    }
    @objc func enableView() {
        self.weexInstance.viewController.view.isUserInteractionEnabled = true
    }
    
    func getContextDetail() -> [String: Any] {
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        let screenWidth = screenSize.width
        let userAgent = "JRAPIManager.sharedManager().userAgent?.stringByEncoding()"
        let ipAddress = "JRUtilities.getIPAddressForCellOrWireless()"
        let operatingSystemVersion = "JRUtilities.getSystemVersionOfDevice().stringByEncoding()"
        let model = "JRUtilities.platformString().stringByEncoding()"
        let hmv = "JRUtilities.hmvString().stringByEncoding()"
        let connectionType = "Reachability.forInternetConnection().userSelectedNetworkType()?.stringByEncoding()"
        let carrierName = "JRV2PostManager.sharedManager.carrierName()"
        let browserName = "UIDevice.current.identifierForVendor?.uuidString.stringByEncoding()"
        let appVersion = "JRUtilities.getAppVersion().stringByEncoding()"
        let channelId = "UAirship.push().channelID"
        let osVersion = "UIDevice.current.systemVersion.stringByEncoding()"
        let siteId = "JRServer.sharedServer().site_id"
        let childSiteId = "JRServer.sharedServer().child_site_id"
        let deviceName = "JRUtilities.platformString().stringByEncoding()"
        let device = "UIDevice.current.name.stringByEncoding()"
        let deviceIdentifier = "JRAPIManager.sharedManager().deviceIdentifier"
        let advertisingId = "JRAPIManager.sharedManager().getIDFAIdentifier()"
        let language = "JRServer.getLanguageCodeForSelectedLanguage().stringByEncoding()"
        let customDeviceId = UIDevice.current.identifierForVendor?.uuidString
        var latitude: Double = 0.0
        var longitude: Double = 0.0
        let locationInfo = "JRLocationManager.shared.currentLocationInfo"
//        if let locationInfo = locationInfo {
//            if let lat = locationInfo["lat"] as? Double,let long = locationInfo["long"] as? Double{
//                latitude = lat
//                longitude = long
//            }
//        }
        let gaId = "JRAPIManager.sharedManager().getIDFAIdentifier()?.stringByEncoding()"
        var userDetail = [String: Any]()
        userDetail["user_id"] = 0
        userDetail["ga_id"] = gaId
        userDetail["experiment_id"] = ""
        
        
        var geoDetail = [String: Any]()
        geoDetail["lat"] = String(describing: latitude)
        geoDetail["long"] = String(describing: longitude)
        
        var trackingDetail = [String: Any]()
        trackingDetail["referer_ui_element"] = ""
        
        var deviceDetail = [String: Any]()
        deviceDetail["ua"] = userAgent
        deviceDetail["ip"] = ipAddress
        deviceDetail["ip_v6"] = ""
        deviceDetail["make"] = "Apple"
        deviceDetail["model"] = model
        deviceDetail["osv"] = operatingSystemVersion
        deviceDetail["hwv"] = hmv
        deviceDetail["connection_type"] = connectionType
        deviceDetail["carrier"] = carrierName
        deviceDetail["aaid"] = ""
        deviceDetail["browser_uuid"] = browserName
        deviceDetail["device_type"] = "PHONE"
        deviceDetail["os"] = "iOS"
        
        var contextDetail = [String: Any]()
        contextDetail["user"] = userDetail
        contextDetail["version"] = appVersion
        contextDetail["geo"] = geoDetail
        contextDetail["device"] = deviceDetail
        contextDetail["channel"] = "APP"
        contextDetail["tracking"] = trackingDetail
        
        var jsonContext = [String: Any]()
        jsonContext["context"] = contextDetail
        
        var contextDic = [String: Any]()
        contextDic["device"] = "iPHONE"
        contextDic["Advertising_ID"] = advertisingId
        contextDic["deviceManufacturer"] = "Apple"
        contextDic["lang_id"] = "1"
        contextDic["Custom_Device_ID"] = customDeviceId
        contextDic["Channel_ID"] = channelId
        contextDic["Device_Brand"] = "Apple"
        contextDic["client"] =  "JRAPIManager.sharedManager().client"
        contextDic["version"] = appVersion
        contextDic["osVersion"] = osVersion
        contextDic["site_id"] = siteId
        contextDic["deviceName"] = deviceName
        contextDic["long"] = longitude
        contextDic["networkType"] = connectionType
        contextDic["child_site_id"] = childSiteId
        contextDic["height"] = Int(screenHeight)
        contextDic["Device_Name"] = device
        contextDic["deviceIdentifier"] = deviceIdentifier
        contextDic["playStore"] = "false"
        contextDic["width"] = Int(screenWidth)
        contextDic["language"] = language
        contextDic["jsonContext"] = jsonContext
        contextDic["lat"] = latitude
        contextDic["cart_id"] = "JRShoppingCartManager.shared().cartId"
        contextDic["sso_token"] = "JRAccount.shared().ssoToken"
        contextDic["addressApiHeader"] = "getAddressApiHeader()"
        
        return contextDic
    }
}
