/**
 * Created by Weex.
 * Copyright (c) 2016, Alibaba, Inc. All rights reserved.
 *
 * This source code is licensed under the Apache Licence 2.0.
 * For the full copyright and license information,please view the LICENSE file in the root directory of this source tree.
 */

#import "WXEventModule.h"

@implementation WXEventModule

@synthesize weexInstance;

WX_EXPORT_METHOD(@selector(log:messages:))
WX_EXPORT_METHOD(@selector(openUrl:urlType:))
WX_EXPORT_METHOD(@selector(sendGAEvent:dict:))
WX_EXPORT_METHOD(@selector(updateGAKey:gaKey:))
WX_EXPORT_METHOD(@selector(getAppContextDetails:))
//WX_EXPORT_METHOD(@selector(openUrlWithMapParams:))
WX_EXPORT_METHOD(@selector(sendGAEventWithMapOnly:))
WX_EXPORT_METHOD(@selector(weexPageScrollEvent:x:y:))
WX_EXPORT_METHOD(@selector(getResponseData:jsonData:))
WX_EXPORT_METHOD(@selector(weexPageTitleChangeEvent:title:))
WX_EXPORT_METHOD(@selector(genericNativeCall:dict:callback:))
WX_EXPORT_METHOD(@selector(showProgressBar:gravity:showProgress:delayMillis:))

@end

