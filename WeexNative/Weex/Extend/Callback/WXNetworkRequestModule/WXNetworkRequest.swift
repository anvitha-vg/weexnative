//
//  WXNetworkRequest.swift
//  Jarvis
//
//  Created by Abhishek on 21/05/18.
//  Copyright © 2018 One97. All rights reserved.
//

import Foundation
import WeexSDK

struct WXNetworkResponse {
    let status: Int
    let data: Any
}

//MARK:- Exposed Methods
public extension WXNetworkRequest {
    
    @objc public func fetch(_ dict: [String: AnyObject], callback: @escaping WXModuleCallback, progressCallback: WXModuleCallback) {
        
        if let request = httpRequest(dict) {
            
            URLSession.shared.dataTask(with: request) { (jsonData, response, error) in
                if let httpResponse = response as? HTTPURLResponse, (httpResponse.statusCode == 200 || httpResponse.statusCode == 412)  {
                    do {
                        if let jsonData = jsonData, let responseJSON = try JSONSerialization.jsonObject(with: jsonData) as? [AnyHashable : Any] {
                            self.handleCallback(status: httpResponse.statusCode, response: responseJSON, callback: callback)
                            return
                        }
                    } catch {
                        self.handleCallback(status: httpResponse.statusCode, response: ["error": "Decoding Error Response"], callback: callback)
                        return
                    }
                } else if let jsonData = jsonData {
                    do {
                        let errorResponse = try JSONDecoder().decode(WXNetworkErrorModel.self, from: jsonData)
                        let errorMessage = """
                        \(errorResponse.errorCode ?? "")
                        \(errorResponse.errorStatus.message.title ?? "")
                        \(errorResponse.errorStatus.message.message ?? "")
                        """
                        self.handleCallback(status: 0, response: ["error": errorMessage], callback: callback)
                            return
                        
                    } catch {
                        do {
                            if let httpResponse = response as? HTTPURLResponse, let responseJSON = try JSONSerialization.jsonObject(with: jsonData) as? [AnyHashable : Any] {
                                self.handleCallback(status: httpResponse.statusCode, response: responseJSON, callback: callback)
                            } else {
                                self.handleCallback(status: 0, response: ["error": "Host not resolved"], callback: callback)
                            }
                        } catch {
                            self.handleCallback(status: 0, response: ["error": "Decoding Error Response"], callback: callback)
                        }
                        
                        return
                    }
                }
                self.handleCallback(status: 0, response: ["error": "Host not resolved"], callback: callback)
            }
        }
    }
    
    
    private func handleCallback(status statusCode: Int, response: Any, callback: @escaping WXModuleCallback ) {
        var responseDict = [String: Any]()
        responseDict["status"] = statusCode
        responseDict["data"] = response
        callback(responseDict)
    }
    
    func httpRequest(_ dict: [String: AnyObject]) -> URLRequest? {
        
        guard let unwrappedUrl = dict["url"] as? String, let url = URL(string: unwrappedUrl) else{
            return nil
        }
        
        var request = URLRequest.init(url: url)
        
        if let method = dict["method"] as? String {
            request.httpMethod = method
        }
        
        if let headerDict = dict["headers"] as? [String: Any] {
            for (key, value) in headerDict {
                if let value = value as? String {
                    request.addValue(value, forHTTPHeaderField: key)
                }
            }
        }
        
        if let method = request.httpMethod, method.isEqualToString(find: "POST") {
            if let body = dict["body"] as? String, body.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0 {
                let data = body.data(using: .utf8)
                request.httpBody = data
            } else if let body = dict["body"] as? [String: Any] {
                let data = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
                request.httpBody = data
            }
        }
        
        if let timeOut = dict["timeout"] as? Int {
            request.timeoutInterval = TimeInterval(timeOut)
        }

        return request
    }
}
