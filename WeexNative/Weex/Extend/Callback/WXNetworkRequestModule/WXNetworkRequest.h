//
//  WXNetworkRequest.h
//  Jarvis
//
//  Created by Abhishek on 21/05/18.
//  Copyright © 2018 One97. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WeexSDK.h>

@interface WXNetworkRequest : NSObject <WXModuleProtocol>

@end
