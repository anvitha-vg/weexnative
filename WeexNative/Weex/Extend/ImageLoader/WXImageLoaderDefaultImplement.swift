//
//  WXImageLoaderDefaultImplement.swift
//  Mall
//
//  Created by zifan.zx on 19/01/2017.
//  Copyright © 2017 One97. All rights reserved.
//

import Foundation
import WeexSDK
import Swift
//import SDWebImage

class WXImageLoaderDefaultImplement:NSObject, WXImgLoaderProtocol {
    
    @objc func downloadImage(withURL url: String!, imageFrame: CGRect, userInfo options: [AnyHashable : Any]! = [:], completed completedBlock: ((UIImage?, Error?, Bool) -> Void)!) -> WXImageOperationProtocol! {
        var newURLString:String
        guard let url = url else {
            return nil
        }
        newURLString = url
        guard var imageURL = URL(string: newURLString) else {
            return nil
        }
        
        imageURL = updatedImageUrl(imageURL, frame: imageFrame)
        #if DEBUG
        print("===================-----------------WEEX IMAGE URLS-------------------=============================")
        print(imageURL)
        #endif
        let operation = SDWebImageManager.shared().downloadImage(with: imageURL, options: SDWebImageOptions.retryFailed, progress: { (receivedSize:Int, expectedSize:Int) in
        })
        {
            (image:UIImage?, error:Error?, cacheType:SDImageCacheType, finished:Bool, imageURL:URL?) in
            if (completedBlock != nil) {
                completedBlock(image, error, finished)
            }
        }
        
        return operation as? WXImageOperationProtocol
    }
    
    @objc func updatedImageUrl(_ url: URL, frame: CGRect) -> URL {
        let baseUrl = url.absoluteString
        let scalingFactor = 0.5
        let isScalingFactor = "false"
        if (isScalingFactor == nil) {
            if let finalImageUrl = getImageUrl(baseUrl, frame: frame, scalingFactor: scalingFactor) {
                return finalImageUrl
            }
        }
        let separatorUrls = baseUrl.split(separator: "?")
        let scalingUrl = separatorUrls[0]
        if let finalImageUrl = getImageUrl(String(scalingUrl), frame: frame, scalingFactor: scalingFactor) {
            return finalImageUrl
        }
        
        return url
    }
    
    private func getImageUrl(_ baseUrl: String, frame: CGRect, scalingFactor: Double) -> URL? {
        var imWidth = 0
        let iphoneScale = UIScreen.main.scale
        imWidth = Int((ceil(Double(frame.size.width * iphoneScale)) / scalingFactor))
        if imWidth > 0 {
            var param = ""
            param = baseUrl.contains(find: "?") ? "&" : "?"
            let imageString = "\(baseUrl)\(param)imwidth=\(String(describing: imWidth))"
            if let finalImageUrl = URL(string: imageString) {
                return finalImageUrl
            }
        }
        return URL(string: baseUrl)
    }
}

